----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;


entity ej01_regd_x2 is
    port ( clk_i, rst_i    : in  std_logic;
           a_i             : in  std_logic;
           y1_o, y2_o      : out std_logic);
end ej01_regd_x2;

architecture rtl of ej01_regd_x2 is


begin

    process (rst_i,clk_i)
    begin    
        if rst_i = '1' then
            y1_o <= '0';
        elsif rising_edge(clk_i) then
            y1_o <= a_i;
        end if;    
    end process;

    process (rst_i,clk_i)
    begin    
        if rising_edge(clk_i) then
            if rst_i = '1' then
                y2_o <= '0';
            else    
                y2_o <= a_i;
            end if;    
        end if;    
    end process;

end rtl;
