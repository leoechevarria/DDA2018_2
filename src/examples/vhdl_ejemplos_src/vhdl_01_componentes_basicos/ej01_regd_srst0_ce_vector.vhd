----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;


entity ej01_regd_srst0_ce_vector is
    port ( clk_i, rst_ni   : in  std_logic;
           ce_i            : in  std_logic;
           a_i             : in  std_logic_vector(11 downto 0);
           y_o             : out std_logic_vector(11 downto 0));
end ej01_regd_srst0_ce_vector;

architecture rtl of ej01_regd_srst0_ce_vector is
    signal rst_int : std_logic;
begin

   rst_int <= not rst_ni;

    process (clk_i)
    begin    
        if rising_edge(clk_i) then
            if rst_int = '1' then
                y_o <= (1 => '1', others => '0');
            elsif ce_i = '1' then
                y_o <= a_i;
            end if;
        end if;    
    end process;
    
end rtl;
