----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ej02_comb02 is
    port ( data_i             : in  std_logic_vector(7 downto 0);
           sel_i              : in  std_logic_vector(2 downto 0);
           y_o                : out std_logic);
end ej02_comb02;

architecture rtl of ej02_comb02 is

begin

    y_o <= data_i(to_integer(unsigned(sel_i)));

end rtl;
