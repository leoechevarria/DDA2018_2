----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos de comportaniento de se�ales con retardos
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity wait_delays is
end wait_delays;

architecture sim of wait_delays is
    constant T : time := 100 ns;
    signal pulso, pdly1, pdly2, pdly3, pdly4, pdly5, pdly6, pdly7 : std_logic;
begin

    pdly1 <= inertial pulso after 1.1*T; -- Rechaza pulsos menores a 1.1*T
    pdly2 <= pulso after 1.1*T; -- Por defecto es inertial 
    pdly3 <= pulso after 2.5*T;
    
    pdly4 <= transport pulso after 1.1*T;
    pdly5 <= transport pulso after 2.5*T;
    
    pdly6 <= reject 2*T inertial pulso after 3.5*T;
    
    pdly7 <= pulso'delayed(3.5*T); -- Usa modelo transport
    
    -- Generacion de pulsos de prueba
    process
    begin
        pulso <= '0';
        wait for 10 * T;
        -- Pulso 1T
        pulso <= '1';
        wait for 1 * T;
        pulso <= '0';
        
        wait for 10 * T; 
        -- Pulso 2T
        pulso <= '1';
        wait for 2 * T;
        pulso <= '0';
        
        wait for 10 * T; 
        -- Pulso 3T
        pulso <= '1';
        wait for 3 * T;
        pulso <= '0';
        
        wait for 10 * T; 
        -- Pulso 4T
        pulso <= '1';
        wait for 4 * T;
        pulso <= '0';
        
        wait for 10 * T; 
        
        -- Pulso 5T
        pulso <= '1';
        wait for 5 * T;
        pulso <= '0';
        wait for 10 * T; 
        
        wait;
    end process;    
      
end sim;
