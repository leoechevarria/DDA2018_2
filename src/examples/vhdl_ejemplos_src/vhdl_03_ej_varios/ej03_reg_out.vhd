----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity ej03_reg_out is
    port ( clk_i, 
           a_i, b_i, c_i  : in  std_logic;
           y1_o, y2_o     : out std_logic);
end ej03_reg_out;

architecture rtl of ej03_reg_out is
    signal y1, y2 : std_logic;
begin
    y1 <= a_i and b_i;
    y2 <= b_i or c_i;

    process(clk_i)
    begin
        if rising_edge(clk_i) then
            y1_o <= y1;
            y2_o <= y2;
        end if;    
    end process;    
end rtl;
