----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity ej03_mux_reg is
    port ( clk_i, 
           a_i, b_i, c_i,
           d_i, e_i       : in  std_logic;
           sel_i          : in  std_logic_vector(2 downto 0);
           y_o            : out std_logic);
end ej03_mux_reg;

architecture rtl of ej03_mux_reg is
begin
    process(clk_i)
    begin
        if rising_edge(clk_i) then
            case sel_i is
                when "000"  => y_o <=  a_i;
                when "001"  => y_o <=  b_i;
                when "010"  => y_o <=  c_i;
                when "011"  => y_o <=  d_i;
                when others => y_o <=  e_i;
            end case;
        end if;    
    end process;    
end rtl;
