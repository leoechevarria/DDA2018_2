----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity ej03_reg_in is
    port ( clk_i, 
           a_i, b_i, c_i  : in  std_logic;
           y1_o, y2_o     : out std_logic);
end ej03_reg_in;

architecture rtl of ej03_reg_in is
    signal a_r, b_r, c_r : std_logic;
begin
    y1_o <= a_r and b_r;
    y2_o <= b_r or c_r;

    process(clk_i)
    begin
        if rising_edge(clk_i) then
            a_r <= a_i;
            b_r <= b_i;
            c_r <= c_i;
        end if;    
    end process;    
end rtl;
