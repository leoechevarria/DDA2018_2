----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ej03_sumas_05 is
    port ( clk_i               : in std_logic; 
           a_i, b_i, c_i, d_i  : in  std_logic_vector(7 downto 0);
           y_o                 : out std_logic_vector(7 downto 0));
end ej03_sumas_05;

architecture rtl of ej03_sumas_05 is
    -- Se�ales unsigned para poder visualizar mejor
    signal a, b, c, d, y : unsigned(7 downto 0); 
begin
    a <= unsigned(a_i);
    b <= unsigned(b_i);
    c <= unsigned(c_i);
    d <= unsigned(d_i);
    y <= (a + d) + (b + c);

    process(clk_i)
    begin
        if rising_edge(clk_i) then
            y_o <= std_logic_vector(y);
        end if;    
    end process;    
end rtl;
