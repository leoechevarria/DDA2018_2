----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ej03_sumas_04b is
    port ( clk_i               : in std_logic; 
           a_i, b_i, c_i, d_i  : in  std_logic_vector(7 downto 0);
           y_o                 : out std_logic_vector(7 downto 0));
end ej03_sumas_04b;

architecture rtl of ej03_sumas_04b is
    
begin

    process(clk_i)
      -- Variables definidas para poder visualizar operaciones
      variable a, b, c, d, y : unsigned(7 downto 0); 
    begin
        if rising_edge(clk_i) then
            a := unsigned(a_i);
            b := unsigned(b_i);
            -- c y d todav�a no est�n definidas!
            y := a + b + c + d;
            c := unsigned(c_i);
            d := unsigned(d_i);
            
            y_o <= std_logic_vector(y);
        end if;    
    end process;    

end rtl;
