----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ej03_sumas_var is
    port ( clk_i               : in std_logic; 
           a_i, b_i, c_i, d_i  : in  std_logic_vector(7 downto 0);
           y_o                 : out std_logic_vector(7 downto 0));
end ej03_sumas_var;

architecture rtl of ej03_sumas_var is
begin

    process(clk_i)
        variable dato1, dato2 : unsigned(7 downto 0);
    begin
        if rising_edge(clk_i) then
            dato1 := unsigned(a_i) + unsigned(b_i);
            dato2 := unsigned(c_i) + unsigned(d_i);
            
            y_o <= std_logic_vector(dato1-dato2);
        end if;    
    end process;    

end rtl;
