----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ej03_sumas_06 is
    port ( clk_i               : in std_logic; 
           a_i, b_i, c_i, d_i  : in  std_logic_vector(7 downto 0);
           y_o                 : out std_logic_vector(7 downto 0));
end ej03_sumas_06;

architecture rtl of ej03_sumas_06 is
begin
    process(clk_i)
    begin
        if rising_edge(clk_i) then
            -- El "casteog" para sumar unsigned no permite 
            -- visualizar claramente la agrupaci�n de operaciones 
            y_o <= std_logic_vector((unsigned(a_i) + unsigned(d_i)) + (unsigned(b_i) + unsigned(c_i)));
        end if;    
    end process;    
end rtl;
