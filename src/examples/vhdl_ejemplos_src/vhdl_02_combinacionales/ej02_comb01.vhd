----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity ej02_comb01 is
    port ( a_i, b_i, c_i, 
           d_i, e_i           : in  std_logic;
           sel_i              : in  std_logic_vector(2 downto 0);
           y_o                : out std_logic);
end ej02_comb01;

architecture rtl of ej02_comb01 is

begin
    -- Instrucci�n concurrente
    with sel_i select
        y_o <=  a_i when "000",
                b_i when "001",
                c_i when "010",
                d_i when "011",
                e_i when others;

end rtl;
