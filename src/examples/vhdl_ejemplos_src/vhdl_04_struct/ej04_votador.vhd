----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity voto2de3 is
    port ( rst_i, clk_i  : in  std_logic;
           a_i, b_i, c_i : in  std_logic;
           y_o           : out std_logic);
end voto2de3;

architecture rtl of voto2de3 is

begin
    -- Votador 2 de 3. No se eval�an los 3 en 1 porque est� 
    --  cubierto por las otraas condiciones
    process (clk_i)
    begin    
        if rising_edge(clk_i) then
            if rst_i = '1' then
                y_o <= '0';
            else
                y_o <= (a_i and b_i) or (b_i and c_i) or (a_i and c_i);
            end if;
        end if;    
    end process;
           
end rtl;
