----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity desplaza_n is
    generic(N : integer := 10);
    port ( rst_i, clk_i    : in  std_logic;
           bit_i, bvalid_i : in  std_logic;  
           data_o          : out std_logic_vector(N-1 downto 0);
           dvalid_o        : out std_logic);
end desplaza_n;

architecture rtl1 of desplaza_n is
    signal data_r    : std_logic_vector(N-1 downto 0);
    signal cnt_bit_r : std_logic_vector(N-1 downto 0);
begin
    -- shift register a la salida
    data_o   <= data_r;
    
    -- Registro de desplazamient con CE. cada N pulsos genera valid  
    -- El contador de bits es shift reg que inicia en "001" y se corre
    -- a la izquierda para sacar "1" cada 3 bits de entrada
    process (clk_i, rst_i)
    begin    
        if rising_edge(clk_i) then
            if rst_i = '1' then
                data_r    <= (others => '0');
                cnt_bit_r <= (0=> '1', others => '0');
                dvalid_o  <= '0';
            else
                dvalid_o <= '0';
                if bvalid_i = '1' then
                    data_r    <= data_r(N-2 downto 0) & bit_i; -- Shift a la izquierda 
                    cnt_bit_r <= cnt_bit_r(N-2 downto 0) & cnt_bit_r(N-1);
                    if cnt_bit_r(N-2) = '1' then
                        dvalid_o <= '1';
                    end if;
                end if;
            end if;
        end if;    
    end process;
           
end rtl1;