----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity reg_ce is
    port ( rst_i, clk_i : in  std_logic;
           ce_i         : in  std_logic;  
           d_i          : in  std_logic;  
           q_o          : out std_logic);
end reg_ce;

architecture rtl of reg_ce is

begin
    
    process (clk_i, rst_i)
    begin    
        if rising_edge(clk_i) then
            if rst_i = '1' then
                q_o    <= '0';
            elsif ce_i = '1' then
                q_o <= d_i;
            end if;
        end if;    
    end process;
           
end rtl;