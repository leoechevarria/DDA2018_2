----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity ej04_top is
    port ( rst_i, clk_i   : in  std_logic;
           bit_i, valid_i : in  std_logic;  
           bit_o, valid_o : out std_logic);
end ej04_top;

architecture rtl of ej04_top is
    signal datos3  : std_logic_vector(2 downto 0);
    signal d3valid : std_logic;
begin
    -- Registro de desplazamiento almacena 3 bits
    U0: entity work.desplaza_n(rtl1)
        generic map(3)
        port map(rst_i, clk_i, bit_i, valid_i, datos3, d3valid);
        
    -- Votador lee 3 bits y cada 3    
    U1: entity work.voto2de3(rtl)
        port map(rst_i => rst_i, clk_i => clk_i, 
            a_i => datos3(2), b_i => datos3(1), c_i => datos3(0), y_o => bit_o);

    -- Registro el valid para sincronizar con el resultado de votador
    U2: entity work.reg_ce(rtl)
        port map(clk_i => clk_i, rst_i => rst_i, d_i => d3valid, q_o => valid_o, ce_i => '1');
        
end rtl;