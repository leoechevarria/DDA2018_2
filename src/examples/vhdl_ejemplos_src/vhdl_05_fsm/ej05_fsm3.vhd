----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;


entity ej05_fsm3 is
    port ( clk_i, rst_i    : in  std_logic;
           a_i             : in  std_logic;
           y_o             : out std_logic);
end ej05_fsm3;

architecture rtl of ej05_fsm3 is

  type estado_type is (COMIENZO, CUENTA1, CUENTA2, CUENTA3);
  signal estado_actual, estado_siguiente : estado_type;
  
  signal y_int : std_logic;
  
begin

    -- estado_actual actual sincronico
    process (clk_i)
    begin    
        if rising_edge(clk_i) then
            if rst_i = '1' then
                estado_actual <= COMIENZO;
            else
                estado_actual <= estado_siguiente;
            end if;
        end if;    
    end process;

    -- estado_actual siguiente y salidas
    process (estado_actual, a_i)
    begin    
        case estado_actual is
            when COMIENZO =>
                y_int <= '0';
                if a_i = '0' then
                    estado_siguiente <= COMIENZO;
                else
                    estado_siguiente <= CUENTA1;
                end if;
            when CUENTA1  =>
                y_int <= '0';
                if a_i = '0' then
                    estado_siguiente <= COMIENZO;
                else
                    estado_siguiente <= CUENTA2;
                end if;
            when CUENTA2  =>
                y_int <= '0';
                if a_i = '0' then
                    estado_siguiente <= COMIENZO;
                else
                    estado_siguiente <= CUENTA3;
                end if;
            when CUENTA3  =>
                y_int <= '1';
                estado_siguiente <= COMIENZO;
            when others   => -- No deberia darse nunca
                y_int <= '0';
                estado_siguiente <= COMIENZO;
        end case;    
    end process;

    -- Salida registrada
    process (clk_i)
    begin    
        if rising_edge(clk_i) then
            if rst_i = '1' then
                y_o <= '0';
            else
                y_o <= y_int;
            end if;
        end if;    
    end process;
            
end rtl;
