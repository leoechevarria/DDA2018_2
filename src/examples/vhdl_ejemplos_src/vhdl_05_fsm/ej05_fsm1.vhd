----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;


entity ej05_fsm1 is
    port ( clk_i, rst_i    : in  std_logic;
           a_i             : in  std_logic;
           y_o             : out std_logic);
end ej05_fsm1;

architecture rtl of ej05_fsm1 is

  type estado_type is (COMIENZO, CUENTA1, CUENTA2, CUENTA3);
  signal estado : estado_type;
  
begin

    process (clk_i)
    begin    
        if rising_edge(clk_i) then
            if rst_i = '1' then
                y_o    <= '0';
                estado <= COMIENZO;
            else
                y_o <= '0';   -- Salida por defecto
                
                -- Analizo estado y entradas y defino salida adecuada
                case estado is
                    when COMIENZO =>
                        if a_i = '0' then
                            estado <= COMIENZO;
                        else
                            estado <= CUENTA1;
                        end if;
                    when CUENTA1  =>
                        if a_i = '0' then
                            estado <= COMIENZO;
                        else
                            estado <= CUENTA2;
                        end if;
                    when CUENTA2  =>
                        if a_i = '0' then
                            estado <= COMIENZO;
                        else
                            estado <= CUENTA3;
                        end if;
                    when CUENTA3  =>
                        y_o <= '1';
                        estado <= COMIENZO;
                    when others   => -- No deberia darse nunca
                        y_o <= '0';
                        estado <= COMIENZO;
                end case;    
            end if;            
        end if;    
    end process;
    
end rtl;
