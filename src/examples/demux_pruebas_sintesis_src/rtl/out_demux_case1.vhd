----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity out_demux_case1 is
    port ( clk_i, rst_i : in  std_logic;
           toggle_i     : in  std_logic;
           lsel_i       : in  std_logic_vector(1 downto 0);
           led_o        : out std_logic_vector(3 downto 0));
end out_demux_case1;

architecture rtl of out_demux_case1 is
    signal toggle_r     : std_logic := '0';    
    signal lsel_r       : std_logic_vector(1 downto 0) := "00";
begin

    -- Registro entradas para ver resultados de timing
    process (clk_i)
    begin    
        if rising_edge(clk_i) then
            lsel_r   <= lsel_i;
            toggle_r <= toggle_i;
        end if;    
    end process;


    process (clk_i)
    begin    
        if rising_edge(clk_i) then
            if rst_i = '1' then
                led_o <= (others => '0');
            else
                case lsel_r is
                    when "00"   =>  led_o <= "000" & toggle_r;
                    when "01"   =>  led_o <= "00" & toggle_r & "0";
                    when "10"   =>  led_o <= "0" & toggle_r & "00";
                    when others =>  led_o <= toggle_r & "000";
                end case;
            end if;    
        end if;    
    end process;
    
end rtl;
