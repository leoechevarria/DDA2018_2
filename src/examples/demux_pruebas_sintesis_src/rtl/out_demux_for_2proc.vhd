----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity out_demux_for_2proc is
    port ( clk_i, rst_i : in  std_logic;
           toggle_i     : in  std_logic;
           lsel_i       : in  std_logic_vector(1 downto 0);
           led_o        : out std_logic_vector(3 downto 0));
end out_demux_for_2proc;

architecture rtl of out_demux_for_2proc is
    signal led_comb : std_logic_vector(3 downto 0);
    
    signal toggle_r     : std_logic := '0';    
    signal lsel_r       : std_logic_vector(1 downto 0) := "00";
begin

    -- Registro entradas para ver resultados de timing
    process (clk_i)
    begin    
        if rising_edge(clk_i) then
            lsel_r   <= lsel_i;
            toggle_r <= toggle_i;
        end if;    
    end process;

    -- Proceso combinacional que define las salidas de maner inmediata 
    --  cuando hay un cambio en toogle_r o lsel_r
    process (lsel_r, toggle_r)
    begin    
        for i in 0 to 3 loop
            if unsigned(lsel_r) = i then
                led_comb(i) <= toggle_r;
            else    
                led_comb(i) <= '0';
            end if;    
        end loop;
    end process;
    
    -- Registro de salida
    process (clk_i)
    begin    
        if rising_edge(clk_i) then
            if rst_i = '1' then
                led_o <= (others => '0');
            else
                led_o <= led_comb;  
            end if;    
        end if;    
    end process;
    
end rtl;
