----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.09.2018 18:05:03
-- Design Name: 
-- Module Name: mux - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux is
    Port ( clk_i    : in STD_LOGIC;
           rst_i    : in STD_LOGIC;
           lsel_i   : in STD_LOGIC_VECTOR (1 downto 0);
           led_o    : out STD_LOGIC_VECTOR (3 downto 0);
           toggle_i : in STD_LOGIC);
end mux;

architecture Behavioral of mux is
    signal toggle_r     : std_logic;    
    signal lsel_r       : std_logic_vector(1 downto 0);
begin

    -- Registro entradas para ver resultados de timing
    process (clk_i)
    begin    
        if rising_edge(clk_i) then
            lsel_r   <= lsel_i;
            toggle_r <= toggle_i;
        end if;    
    end process;


    mux_proc : process(clk_i) 
    begin
        if ( rising_edge(clk_i) ) then
            if ( rst_i = '1' ) then
                led_o <=  (others => '0');
            else
                if (toggle_r = '1') then
                    case lsel_r is
                        when "00"   => led_o <= "0001";
                        when "01"   => led_o <= "0010";
                        when "10"   => led_o <= "0100";
                        when "11"   => led_o <= "1000";
                        when others => led_o <= (others => '0');
                    end case;
                else
                    led_o <= (others => '0');
                end if;
           end if;
        end if;
    end process mux_proc;

end Behavioral;
