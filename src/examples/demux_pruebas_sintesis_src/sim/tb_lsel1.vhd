----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity tb_lsel1 is
    port ( clk_i : in  std_logic;
           lsel_o  : out std_logic_vector(1 downto 0));
end tb_lsel1;

architecture sim of tb_lsel1 is

begin

    process
    begin
        lsel_o <= "00";
        wait for 200 ns;

        lsel_o <= "00";
        wait for 75 ns;
        lsel_o <= "01";
        wait for 75 ns;
        lsel_o <= "10";
        wait for 75 ns;
        lsel_o <= "11";
        wait for 75 ns;
        
        lsel_o <= "00";
        wait for 50 ns;
        lsel_o <= "01";
        wait for 50 ns;
        lsel_o <= "10";
        wait for 50 ns;
        lsel_o <= "11";
        wait for 50 ns;

        lsel_o <= "00";
        wait for 200 ns;
        lsel_o <= "01";
        wait for 200 ns;
        lsel_o <= "10";
        wait for 200 ns;
        lsel_o <= "11";
        wait for 200 ns;
        wait;
    end process;    
      
end sim;
