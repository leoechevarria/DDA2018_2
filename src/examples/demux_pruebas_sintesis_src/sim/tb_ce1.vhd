----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity tb_ce1 is
    port ( clk_i : in  std_logic;
           ce_o  : out std_logic);
     
         
end tb_ce1;

architecture sim of tb_ce1 is

begin

    process
    begin
        ce_o <= '0';
        wait for 200 ns;

        ce_o <= '1';
        wait for 300 ns;
        
        ce_o <= '0';
        wait for 200 ns;

        for i in 1 to 10 loop
            wait until rising_edge(clk_i);
            ce_o <= '1';
            wait until rising_edge(clk_i);
            ce_o <= '0';
            wait for 80 ns;
        end loop;
        ce_o <= '0';
        wait;
    end process;    
      
end sim;
