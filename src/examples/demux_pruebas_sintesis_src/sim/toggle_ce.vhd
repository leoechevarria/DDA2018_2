----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity toggle_ce is
    port ( clk_i, rst_i    : in  std_logic;
           ce_i            : in  std_logic;
           toggle_o        : out std_logic);
end toggle_ce;

architecture rtl of toggle_ce is
    signal toggle_r : std_logic := '0';
begin

    toggle_o <= toggle_r;
    
    process (clk_i)
    begin    
        if rising_edge(clk_i) then
            if rst_i = '1' then
                toggle_r <= '0';
            elsif ce_i = '1' then
                toggle_r <= not toggle_r;
            end if;    
        end if;    
    end process;
    
end rtl;
