----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity tb_demux_tests_top is
end    tb_demux_tests_top;

architecture sim of tb_demux_tests_top is

    signal clk, rst, ce, toggle              : std_logic;
    signal lsel                              : std_logic_vector(1 downto 0);
    signal leds1, leds2, leds3, leds4, leds5, leds6  : std_logic_vector(3 downto 0);
    
  begin
    
    U1: entity work.tb_clk(sim)
          generic map(T_SIM_LIMIT  => 1000 ms,
                      T_CLK_PERIOD => 10 ns)
          port map(clk_o => clk);

    U2: entity work.tb_rst(sim)
          generic map(RST_ACTIVO => '1', T_RST_INICIO => 1 ps, T_RST_ACTIVO => 60 ns)
          port map(rst_async_o => open, rst_sync_o => rst, clk_i => clk);

    U3: entity work.tb_ce1(sim)
          port map(clk_i => clk, ce_o => ce);
        
    U4: entity work.toggle_ce(rtl)
      port map (
        rst_i    => rst,
        clk_i    => clk,
        ce_i     => ce,
        toggle_o   => toggle);
        
    U5: entity work.tb_lsel1(sim)
      port map (
        clk_i   => clk, 
        lsel_o  => lsel);
        
    DUT1: entity work.out_demux_case1(rtl)
      port map (
        rst_i    => rst,
        clk_i    => clk,
        toggle_i => toggle,
        lsel_i   => lsel,
        led_o    => leds1);

    DUT2: entity work.out_demux_case_const1(rtl)
      port map (
        rst_i    => rst,
        clk_i    => clk,
        toggle_i => toggle,
        lsel_i   => lsel,
        led_o    => leds2);

    DUT3: entity work.out_demux_case_const2(rtl)
      port map (
        rst_i    => rst,
        clk_i    => clk,
        toggle_i => toggle,
        lsel_i   => lsel,
        led_o    => leds3);
       
    DUT4: entity work.out_demux_for_1proc(rtl)
      port map (
        rst_i    => rst,
        clk_i    => clk,
        toggle_i => toggle,
        lsel_i   => lsel,
        led_o    => leds4);        
        
    DUT5: entity work.out_demux_for_2proc(rtl)
          port map (
            rst_i    => rst,
            clk_i    => clk,
            toggle_i => toggle,
            lsel_i   => lsel,
            led_o    => leds5);
            
    DUT6: entity work.mux(Behavioral)
                  port map (
                    rst_i    => rst,
                    clk_i    => clk,
                    toggle_i => toggle,
                    lsel_i   => lsel,
                    led_o    => leds6);
end sim;
