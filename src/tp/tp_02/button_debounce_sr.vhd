-- The entire notice above must be reproduced on all authorized copies.

-- File         : button_debounce_sr.vhd
-- Library      : 
-- Company      : Instituto Balseiro (Based on core by EmTech S.A.)
-- Author       : Guillermo Guichal
-- Address      : <guillermo.guichal@ib.edu.ar>
-- Created on   : 16:39:12-28/12/2009
-- Version      : 1.00
-- Description  : Samples input at enable_i rate (if enable_i is active samples at 
--                clk_i rate). Shifts data into SR and detects when appropriate pattern 
--                is detected.
--                Pattern for RE is     

-- Modification History:
-- Date        By    Version    Change Description
-------------------------------------------------------------------------------
-- 28/12/2009  GER      0.10    Original
-- 16/09/2013  MAR      0.20    Modified rising/falling pattern detection
-- 14/04/2018  GEG      1.00    Change for srst only
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity button_debounce_sr is
	generic( 
		BTN_ACTIVE    : std_logic := '1';
		SHIFTER_WIDTH : integer   := 16;
		MODE_POST_RE  : boolean   := TRUE;
		MODE_POST_FE  : boolean   := TRUE);
	port( 
		-- system
		clk_i      : in     std_logic;
		srst_i     : in     std_logic;
		-- control
		enable_i   : in     std_logic  := '1';
		-- data
		data_i     : in     std_logic;
		level_o    : out    std_logic;
		pulse_re_o : out    std_logic;
		pulse_fe_o : out    std_logic);

end button_debounce_sr ;

architecture rtl of button_debounce_sr is

	constant BTN_INACTIVE : std_logic := not BTN_ACTIVE;
	-- Functions
	-- check if there is a rising edge condition
	function is_rising(shifter : std_logic_vector) return std_logic is
		constant LEN : integer := shifter'length;
		variable condition : std_logic_vector(LEN-1 downto 0);
		variable result : std_logic := '0';
	begin
		-- set condition
		if (MODE_POST_RE = TRUE) then
		  -- is rising when MSB is '0' and others '1' 
		  condition(LEN-1) := BTN_INACTIVE;
		  condition(LEN-2 downto 0) := (others => BTN_ACTIVE);
		else
		  -- is rising when LSB is ACTIVE and others INACTIVE
		  condition(LEN-1 downto 1) := (others => BTN_INACTIVE);
		  condition(0) := BTN_ACTIVE;
		end if;

		-- test condition
		if (shifter = condition) then
		  result := '1';
		end if;
		return result;
	end is_rising;

	-- check if there is a falling edge condition
	function is_falling(shifter : std_logic_vector) return std_logic is
		constant LEN : integer := shifter'length;
		variable condition : std_logic_vector(LEN-1 downto 0);
		variable result : std_logic := '0';
	begin
	
		-- set condition
		if (MODE_POST_FE = TRUE) then
			-- is falling when MSB is ACTIVE and others NOT ACTIVE
			condition(LEN-1) := BTN_ACTIVE;
			condition(LEN-2 downto 0) := (others => BTN_INACTIVE);
		else
			-- is falling when LSB is '0' and others '1'
			condition(LEN-1 downto 1) := (others => BTN_ACTIVE);
			condition(0) := BTN_INACTIVE;
		end if;
		if (shifter = condition) then
			result := '1';
		end if;
		return result;
	end is_falling;

	-- Signals
	signal pulse_re_r : std_logic;
	signal pulse_fe_r : std_logic;
	signal level_r    : std_logic;
	signal data_s     : std_logic;
	signal shifter_r  : std_logic_vector(SHIFTER_WIDTH-1 downto 0);
begin

	-- register input data
	pulse_re_o <= pulse_re_r;
	pulse_fe_o <= pulse_fe_r;
	level_o    <= level_r;

	-- insert data_i into shifter with each pulse of enable_i
	shifter_proc : process(clk_i)
	begin
		if (rising_edge(clk_i)) then
			if (srst_i = '1') then
				pulse_re_r <= '0';
				pulse_fe_r <= '0';
				data_s <= BTN_INACTIVE;
				shifter_r <= (others => BTN_INACTIVE);
				level_r   <= BTN_INACTIVE;
			else
				pulse_re_r <= '0';
				pulse_fe_r <= '0';							
				if (enable_i = '1') then
					-- syncronize input data and insert into shift register
					data_s <= data_i;
					shifter_r <= shifter_r(SHIFTER_WIDTH-2 downto 0) & data_s;
					-- generate pulses
					if (is_rising(shifter_r) = '1' and level_r = '0') then
					pulse_re_r <= '1';
					level_r    <= '1';
					end if;
					if (is_falling(shifter_r) = '1' and level_r = '1') then
						pulse_fe_r <= '1';
						level_r    <= '0';
					end if;
				end if;
			end if;  
		end if;
    end process;

end architecture rtl;
