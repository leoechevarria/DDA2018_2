library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity signal_power_axis is
  generic (
    INPUT_WIDTH       : integer := 16;
    OUTPUT_WIDTH      : integer := 8;
    DELAY_LENGTH      : integer := 512;
    DELAY_LENGTH_BITS : integer := 9);
  port (
    clk_i        : in  std_logic;
    i_tdata      : in  std_logic_vector(INPUT_WIDTH-1 downto 0);
    i_tvalid     : in  std_logic;
    i_tready     : out std_logic;
    o_tdata      : out std_logic_vector(OUTPUT_WIDTH-1 downto 0);
    o_tvalid     : out std_logic;
    o_tready     : in  std_logic);
end signal_power_axis;


architecture rtl of signal_power_axis is

constant SCALE_OFFSET : integer := 6;

signal i_tdata_r            : signed(INPUT_WIDTH-1 downto 0):= (others => '0');
signal i_tdata_squared_r    : signed(2*INPUT_WIDTH - 1 downto 0):= (others => '0');

signal shift_0_r, shift_1_r : std_logic := '0';
signal i_tready_int         : std_logic := '0';

signal accumulator          : signed(2*INPUT_WIDTH+DELAY_LENGTH_BITS-1 downto 0) := (others => '0');
type ram_type is array (0 to DELAY_LENGTH-3) of signed(2*INPUT_WIDTH-1 downto 0);
signal delay_line           : ram_type := (others => (others => '0'));
signal delay_tmp0_r         : signed(2*INPUT_WIDTH-1 downto 0) := (others => '0');
signal delay_tmp1_r         : signed(2*INPUT_WIDTH-1 downto 0) := (others => '0');
signal delay_counter_r      : unsigned(DELAY_LENGTH_BITS-1 downto 0) := (others => '0');

attribute ram_style: string;
attribute ram_style of delay_line : signal is "block";

begin

i_tready     <= i_tready_int;
i_tready_int <= o_tready;

--power
process (clk_i) 
begin
  if rising_edge (clk_i) then
    i_tdata_r         <= signed(i_tdata);
    i_tdata_squared_r <= i_tdata_r*i_tdata_r;
    
    shift_0_r <= '0';
    shift_1_r <= shift_0_r;
    
    if (i_tvalid = '1' and i_tready_int = '1') then
      shift_0_r <= '1';
    end if;
  end if;
end process;

--accumulator
process (clk_i)
begin
  if rising_edge (clk_i) then
    if (shift_1_r = '1') then
      accumulator <= accumulator - delay_tmp1_r + i_tdata_squared_r;
    end if;
end if;
end process;

--delay line 
process (clk_i)
begin
  if rising_edge (clk_i) then
    if (shift_0_r = '1') then
      delay_counter_r <= delay_counter_r + 1;
      
      if (delay_counter_r = (DELAY_LENGTH - 3)) then
        delay_counter_r <= (others => '0');
      end if;
      
      delay_line(to_integer(delay_counter_r)) <= i_tdata_squared_r;
      delay_tmp0_r <= delay_line(to_integer(delay_counter_r));
      delay_tmp1_r <= delay_tmp0_r;
    end if;
end if;
end process;

--output assignments
o_tvalid         <= shift_1_r;
o_tdata          <= std_logic_vector(accumulator(accumulator'length-SCALE_OFFSET downto accumulator'length-OUTPUT_WIDTH-(SCALE_OFFSET-1)));

end rtl;
