library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use STD.textio.all;
use ieee.std_logic_textio.all;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;


entity tb_signal_power_axis is
end tb_signal_power_axis;

architecture sim of tb_signal_power_axis is

signal clk : std_logic := '0';
signal o_tvalid, o_tready, i_tvalid, i_tready : std_logic;
signal i_tdata : std_logic_vector(15 downto 0);
signal o_tdata : std_logic_vector(7 downto 0);

signal i_tdata_tmp : unsigned(15 downto 0) := (others => '0');

constant CLOCK_PERIOD : time := 10 ns;

signal SIM_ENDED : std_logic := '0';

begin

i_tdata <= std_logic_vector(i_tdata_tmp);

dut: entity work.signal_power_axis
  generic map (
    INPUT_WIDTH       => i_tdata'length,
    OUTPUT_WIDTH      => o_tdata'length,
    DELAY_LENGTH      => 8192,
    DELAY_LENGTH_BITS => 16
  )
	port map(
	 clk_i    => clk,
	 i_tdata  => i_tdata,
	 i_tvalid => i_tvalid,
	 i_tready => i_tready,
	 o_tdata  => o_tdata,
	 o_tvalid => o_tvalid,
	 o_tready => o_tready
);

clk <= not clk after CLOCK_PERIOD/2 when SIM_ENDED /= '1' else '0';
    
pulso : process
begin
    o_tready <= '1';
    i_tvalid <= '0';
    
    wait for 100 ns;
    
    for i in 1 to 100 loop
      for j in 1 to 200 loop
        i_tdata_tmp <= x"c000";
        i_tvalid    <= '1';
        wait until rising_edge(clk);
        i_tvalid    <= '0';
        wait for 90 ns;
      end loop;
    
      for j in 1 to 200 loop
        i_tdata_tmp <= x"3FFF";
        i_tvalid    <= '1';
        wait until rising_edge(clk);
        i_tvalid    <= '0';
        wait for 90 ns;
      end loop;
    end loop;
    
    for i in 1 to 100 loop
      for j in 1 to 200 loop
        i_tdata_tmp <= x"0000";
        i_tvalid    <= '1';
        wait until rising_edge(clk);
        i_tvalid    <= '0';
        wait for 90 ns;
      end loop;
    
      for j in 1 to 200 loop
        i_tdata_tmp <= x"7FFF";
        i_tvalid    <= '1';
        wait until rising_edge(clk);
        i_tvalid    <= '0';
        wait for 90 ns;
      end loop;
    end loop;
    
    for i in 1 to 100 loop
      for j in 1 to 200 loop
        i_tdata_tmp <= x"0000";
        i_tvalid    <= '1';
        wait until rising_edge(clk);
        i_tvalid    <= '0';
        wait for 90 ns;
      end loop;
    
      for j in 1 to 200 loop
        i_tdata_tmp <= x"00FF";
        i_tvalid    <= '1';
        wait until rising_edge(clk);
        i_tvalid    <= '0';
        wait for 90 ns;
      end loop;
    end loop;    
      
    for i in 1 to 100 loop
      for j in 1 to 400 loop
        i_tdata_tmp <= x"7FFF";
        i_tvalid    <= '1';
        wait until rising_edge(clk);
        i_tvalid    <= '0';
        wait for 90 ns;
      end loop;
    end loop;       
      
      
    wait for 500 ns;
    
    SIM_ENDED <= '1';
    wait;
end process;  

end sim;