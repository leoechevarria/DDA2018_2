library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use STD.textio.all;
use ieee.std_logic_textio.all;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;


entity tb_fir is
end tb_fir;

architecture sim of tb_fir is

signal clk : std_logic := '0';
signal o_tvalid, o_tready, i_tvalid, i_tready : std_logic;
signal i_tdata : std_logic_vector(15 downto 0);
signal o_tdata : std_logic_vector(15 downto 0);

signal i_tdata_tmp : unsigned(15 downto 0);
signal o_tdata_tmp : std_logic_vector(15 downto 0);

constant CLOCK_PERIOD : time := 10 ns;

signal SIM_ENDED : std_logic := '0';

begin

i_tdata <= std_logic_vector(i_tdata_tmp);

dut: entity work.fir_filter_wrapper
	port map(
    M_AXIS_DATA_0_tdata(15 downto 0) => o_tdata,
    M_AXIS_DATA_0_tvalid => o_tvalid,
    M_AXIS_DATA_0_tready => o_tready,
    S_AXIS_DATA_0_tdata(15 downto 0) => i_tdata,
    S_AXIS_DATA_0_tready => i_tready,
    S_AXIS_DATA_0_tvalid => i_tvalid,
    aclk_0 => clk
);

clk <= not clk after CLOCK_PERIOD/2 when SIM_ENDED /= '1' else '0';
    
pulso : process
begin
    o_tready <= '1';
    i_tvalid <= '0';
    
    wait for 100 ns;
    
    for i in 1 to 2000 loop
      i_tdata_tmp <= x"0000";
      i_tvalid    <= '1';
      wait until rising_edge(clk);
      i_tvalid    <= '0';
      wait for 1000 ns;
    
      i_tdata_tmp <= x"FFFF";
      i_tvalid    <= '1';
      wait until rising_edge(clk);
      i_tvalid    <= '0';
      wait for 1000 ns;
    end loop;
      
    SIM_ENDED <= '1';
    wait;
end process;    

process (clk)
begin
  if rising_edge (clk) then
    if (o_tvalid = '1') then
      o_tdata_tmp <= o_tdata;
    end if;
  end if;
end process;

end sim;