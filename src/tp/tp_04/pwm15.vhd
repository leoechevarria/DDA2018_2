----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: pwm15.vhd
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pwm15 is
  generic (
    FIXED_PW : natural := 7); -- From 0 to 15 (0 = 0%, 15=100%)
  port ( 
    clk_i       : in  std_logic;
    srst_i      : in std_logic;
    ce_15th_i   : in std_logic;
    pwm_o       : out std_logic);
end pwm15;

architecture rtl of pwm15 is

    signal pwm_contador_r : unsigned(3 downto 0); 

begin
    
  -- Contador para base de ciclo PWM, dividido en 15 ciclos iguales.
  -- Usa de base de tiempo el CE de entrada, y cuenta de 0 a 14, dividiendo el ciclo
  -- PWM en 15 partes iguales. Cada 15 pulsos de CE es un ciclo PWM. 
  process (clk_i)
  begin    
    if rising_edge(clk_i) then
      if srst_i = '1' then
        pwm_contador_r <= (others => '0');
      elsif ce_15th_i = '1' then	
        pwm_contador_r <= pwm_contador_r + 1;
        
        if pwm_contador_r = 14 then
          pwm_contador_r <= (others => '0');<        
        end if;     
      end if;
    end if;    
  end process;

  -- Salida en alto si la cuenta es menor al seteo de entrada
  -- Se debe usar un bit mas en el contador para poder indicar 
  -- de 0 a 16 muestras en alto (17 valores posibles)
  process (clk_i)
  begin    
    if rising_edge(clk_i) then
      if srst_i = '1' then
        pwm_o  <= '0';
      else 	
        pwm_o  <= '0';
      
        if pwm_contador_r < FIXED_PW   then
          pwm_o  <= '1';
        end if;	
      end if;
    end if;    
  end process;
    
end rtl;
