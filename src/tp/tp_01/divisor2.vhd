--declaración de librerías
library ieee;
use ieee.std_logic_1164.all;
--use ieee.numeric_std.all;

entity divisor2 is --aquí 'divisor2' es el nombre de la entidad
port (
	-- entradas
	clk_i        : in std_logic;
	rst_i        : in std_logic;
	-- salidas
	ce_o         : out std_logic  --el último elemento de la lista de puertos no lleva ';'
);
end entity divisor2;

architecture rtl of divisor2 is

--aquí aparecerá la declaración de señales y constantes
signal ce_reg : std_logic := '0';

--aquí aparecerán las declaraciones de bloques (ninguna en este caso)

begin

--aquí aparecerán las instanciaciones de bloques (ninguna en este caso)

--aquí podremos incluir toda lógica combinacional
ce_o <= ce_reg;

--processes que cumplen funciones específicas:

divisor_proc : process(clk_i, rst_i)  --'divisor_proc' es el nombre del process
begin
	if ( rst_i = '1' ) then
		ce_reg       <= '0';
	elsif ( rising_edge(clk_i) ) then
		ce_reg <= not ce_reg;
	end if;
end process divisor_proc;


end architecture rtl;
