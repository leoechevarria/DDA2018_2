-- The entire notice above must be reproduced on all authorized copies.

-- File         : uart_baudrate_gen_rtl.vhd
-- Library      : et_generic_lib
-- Company      : EmTech
-- Author       : Gast�n Rodriguez
-- Address      : <grodriguez@emtech.com.ar>
-- Created on   : 17:45:05-10/13/2009
-- Version      : 0.10
-- Description  : Generate baudrate signals to use in a tx/rx uart. Max count
--                and reference clock can be set with input signals.
--                The max_count input is used to obtain the fast_baud signal, 
--                after divide the reference clock. slow_baud is a number of 
--                times slower, to allow sample a bit several times

-- Modification History:
-- Date        By    Version    Change Description
-------------------------------------------------------------------------------
-- 10/13/2009  GER      0.10    Original
-- 06/02/2013  GER      0.11    We change (max_count_i-1) to (max_count_i) to 
--                              make the subtraction manually and save up 
--                              logic.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity uart_baud_gen is
  generic( 
    BIT_SAMPLES : integer := 16;
    COUNT_WIDTH : integer := 16
  );
  port( 
    -- system
    clk_i       : in     std_logic;
    rst_ni      : in     std_logic;
    srst_i      : in     std_logic  := '0';
    -- control
    clk_ref_i   : in     std_logic  := '1';
    max_count_i : in     std_logic_vector (COUNT_WIDTH-1 downto 0);
    -- baudrate
    fast_baud_o : out    std_logic;
    slow_baud_o : out    std_logic
  );

-- Declarations

end uart_baud_gen ;

architecture rtl of uart_baud_gen is
  signal fast_count_r : integer range 0 to 2**COUNT_WIDTH-1;
  signal fast_baud_r  : std_logic;
  signal slow_count_r : integer range 0 to BIT_SAMPLES-1;
  signal slow_baud_r  : std_logic;
begin

  -- generate fast baudrate
  fast_baud_o <= fast_baud_r;
  
  fast_baud_proc : process(rst_ni, clk_i)
  begin
    if (rst_ni = '0') then
      fast_count_r <= 0;
      fast_baud_r <= '0';
    elsif (rising_edge(clk_i)) then
      fast_baud_r <= '0';
      if (srst_i = '1') then
        fast_baud_r <= '1';
        fast_count_r <= 0;
      elsif (clk_ref_i = '1') then
        if (fast_count_r < max_count_i) then
          fast_count_r <= fast_count_r + 1;
        else
          fast_baud_r <= '1';
          fast_count_r <= 0;
        end if;
      end if;
    end if;
  end process;
  
  -- generate baudrate slow
  slow_baud_o <= slow_baud_r;
  
  slow_baud_proc : process(rst_ni, clk_i)
  begin
    if (rst_ni = '0') then
      slow_count_r <= 0;
      slow_baud_r <= '0';
    elsif (rising_edge(clk_i)) then
      slow_baud_r <= '0';
      if (srst_i = '1') then
        slow_baud_r <= '1';
        slow_count_r <= 0;
      elsif (fast_count_r >= max_count_i) then
        if (slow_count_r < BIT_SAMPLES-1) then
          slow_count_r <= slow_count_r + 1;
        else
          slow_baud_r <= '1';
          slow_count_r <= 0;
        end if;
      end if;
    end if;
  end process;
  
end architecture rtl;
