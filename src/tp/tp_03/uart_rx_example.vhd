library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--UART BAUD GEN genera los pulsos para muestrear los datos provenientes de la UART a una tasa UART_BIT_SAMPLES mayor
--a la tasa de UART. Por lo tanto, UART_BIT_SAMPLES*uart_baud_div_i debe ser igual a la cantidad de ciclos de reloj 
--en el clock de sistema que tarda en entrar un dato UART (o lo más cercano)
--Para un reloj de 100 MHz, UART a 9600 bps y 16 samples por bit de UART (153,6 kHz de sampling rate)
--deberíamos dividir el reloj por 651.04 --> 651 = x028B
--uart_baud_div_i es una entrada en lugar de un genérico para poder cambiar la tasa de UART al vuelo

entity uart_rx_example is
  generic( 
    UART_BIT_SAMPLES     : integer := 16;
    UART_DATA_WIDTH      : integer := 8;
    UART_COUNT_WIDTH     : integer := 16
  );                
  port(             
    -- system       
    clk_i           : in     std_logic;
    rst_ni          : in     std_logic;
    srst_i          : in     std_logic := '0';
    
    uart_baud_div_i : in     std_logic_vector(UART_COUNT_WIDTH-1 downto 0) := x"028B";
    -- control      
    enable_i        : in     std_logic := '1';
    ready_o         : out    std_logic;
    -- data
    serial_i        : in     std_logic;
    data_o          : out    std_logic_vector(UART_DATA_WIDTH-1 downto 0)
  );
end entity uart_rx_example;

architecture rtl of uart_rx_example is

-- signals and constants
signal fast_baud : std_logic;

begin
  
  u_uart_baud_gen : entity work.uart_baud_gen
    generic map (
      BIT_SAMPLES => UART_BIT_SAMPLES,
      COUNT_WIDTH => UART_COUNT_WIDTH
    )
    port map (
      clk_i  => clk_i,
      rst_ni => rst_ni,
      srst_i => srst_i,
      
      clk_ref_i => '1',
      max_count_i => uart_baud_div_i,
      
      fast_baud_o => fast_baud,
      slow_baud_o => open
    );
  
  u_uart_rx : entity work.uart_rx
    generic map (
      BIT_SAMPLES => UART_BIT_SAMPLES,
      DATA_WIDTH  => UART_DATA_WIDTH
    )
    port map (
      clk_i       => clk_i,      
      rst_ni      => rst_ni,     
      srst_i      => srst_i,    
      enable_i    => enable_i,   
      fast_baud_i => fast_baud,
      ready_o     => ready_o,    
      error_o     => open, -- ignore failed data received (not parity)    
      serial_i    => serial_i,   
      data_o      => data_o     
    );
  
end architecture rtl;
