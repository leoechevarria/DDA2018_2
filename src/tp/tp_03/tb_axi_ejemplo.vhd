----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity tb_axi_ejemplo is
end  tb_axi_ejemplo;

architecture sim of  tb_axi_ejemplo is

    signal clk, rst_n : std_logic;
    
    signal axi_wstrb                : std_logic_vector(3 downto 0);
    signal axi_bresp, axi_rresp   : std_logic_vector(1 downto 0);
    signal axi_rdata, axi_wdata   : std_logic_vector(31 downto 0);
    signal axi_arprot, axi_awprot : std_logic_vector(2 downto 0);         
    signal axi_araddr, axi_awaddr : std_logic_vector(31 downto 0); 
    signal axi_awvalid, axi_awready, axi_wready, axi_wvalid, axi_bready, 
           axi_arvalid, axi_arready, axi_rready  : std_logic;
    signal axi_bvalid : std_logic := '0'; -- Inicio en 0 para simulaciones de esclavo
    signal axi_rvalid : std_logic := '0'; -- Inicio en 0 para simulaciones de esclavo
    
  begin
    
    U1: entity work.tb_clk(sim)
          port map(clk_o => clk);

    U2: entity work.tb_rst(sim)
          generic map(RST_ACTIVO => '0', T_RST_INICIO => 0 ns, T_RST_ACTIVO => 60 ns)
          port map(rst_async_o => open, rst_sync_o => rst_n, clk_i => clk);
        
    -- Master 
    DUT: entity work.axi_mst_ejemplo(rtl) -- para simulacion RTL
--    DUT: entity work.axi_mst_m02(STRUCTURE) -- Para simulacion post synthesis
      port map(
        
        M_AXI_ACLK    => clk, 
        M_AXI_ARESETN => rst_n, 

        M_AXI_AWADDR  => axi_awaddr,   -- out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        M_AXI_AWPROT  => axi_awprot,   -- out std_logic_vector(2 downto 0);
                         
        M_AXI_AWVALID => axi_awvalid,  -- out std_logic;
        M_AXI_AWREADY => axi_awready,  -- in std_logic;
        M_AXI_WDATA   => axi_wdata,    -- out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        M_AXI_WSTRB   => axi_wstrb,    -- out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
        M_AXI_WVALID  => axi_wvalid,   -- out std_logic;
        M_AXI_WREADY  => axi_wready,   -- in std_logic;
        M_AXI_BRESP   => axi_bresp,    -- in std_logic_vector(1 downto 0);
        M_AXI_BVALID  => axi_bvalid,   -- in std_logic;
        M_AXI_BREADY  => axi_bready,   -- out std_logic;
        M_AXI_ARADDR  => axi_araddr,   -- out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        M_AXI_ARPROT  => axi_arprot,   -- out std_logic_vector(2 downto 0);
        M_AXI_ARVALID => axi_arvalid,  -- out std_logic;
        M_AXI_ARREADY => axi_arready,  -- in std_logic;
        M_AXI_RDATA   => axi_rdata,    -- in std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        M_AXI_RRESP   => axi_rresp,    -- in std_logic_vector(1 downto 0);
        M_AXI_RVALID  => axi_rvalid,   -- in std_logic;
        M_AXI_RREADY  => axi_rready ); -- out std_logic
            
    -- Para probar el Master 02 se usa un esclavo generado con Vivado con 8 registros
    SLV: entity work.axi_slv_ejemplo(rtl)
      generic map(
        C_S_AXI_DATA_WIDTH  => 32,
        C_S_AXI_ADDR_WIDTH  => 5)
      port map(
        S_AXI_ACLK    => clk,
        S_AXI_ARESETN => rst_n,
        S_AXI_AWADDR  => axi_awaddr(4 downto 0),   -- in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
        S_AXI_AWPROT  => axi_awprot,   -- in std_logic_vector(2 downto 0);
        S_AXI_AWVALID => axi_awvalid,  -- in std_logic;
        S_AXI_AWREADY => axi_awready,  -- out std_logic;
        S_AXI_WDATA   => axi_wdata,    -- in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        S_AXI_WSTRB   => axi_wstrb,    -- in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
        S_AXI_WVALID  => axi_wvalid,   -- in std_logic;
        S_AXI_WREADY  => axi_wready,   -- out std_logic;
        S_AXI_BRESP   => axi_bresp,    -- out std_logic_vector(1 downto 0);
        S_AXI_BVALID  => axi_bvalid,   -- out std_logic;
        S_AXI_BREADY  => axi_bready,   -- in std_logic;
        S_AXI_ARADDR  => axi_araddr(4 downto 0),   -- in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
        S_AXI_ARPROT  => axi_arprot,   -- in std_logic_vector(2 downto 0);
        S_AXI_ARVALID => axi_arvalid,  -- in std_logic;
        S_AXI_ARREADY => axi_arready,  -- out std_logic;
        S_AXI_RDATA   => axi_rdata,    -- out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        S_AXI_RRESP   => axi_rresp,    -- out std_logic_vector(1 downto 0);
        S_AXI_RVALID  => axi_rvalid,   -- out std_logic;
        S_AXI_RREADY  => axi_rready);

    

end sim;
