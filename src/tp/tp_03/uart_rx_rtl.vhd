-- The entire notice above must be reproduced on all authorized copies.

-- File         : uart_rx_rtl.vhd
-- Library      : et_generic_lib
-- Company      : EmTech
-- Author       : Gast�n Rodriguez
-- Address      : <grodriguez@emtech.com.ar>
-- Created on   : 14:58:25-10/14/2009
-- Version      : 0.10
-- Description  : ...

-- Modification History:
-- Date        By    Version    Change Description
-------------------------------------------------------------------------------
-- 10/14/2009  GER      0.10    Original
-- 11/27/2009  GER      0.11    Signal 'sample_bit' made combinatorial to allow
--                              faster baudrates and to sample exactly in the
--                              middle of the bit
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity uart_rx is
  generic( 
    BIT_SAMPLES : integer := 16;
    DATA_WIDTH  : integer := 8
  );
  port( 
    -- system
    clk_i       : in     std_logic;
    rst_ni      : in     std_logic;
    srst_i      : in     std_logic  := '0';
    -- control
    enable_i    : in     std_logic;
    fast_baud_i : in     std_logic;
    ready_o     : out    std_logic;
    error_o     : out    std_logic;
    -- data
    serial_i    : in     std_logic;
    data_o      : out    std_logic_vector (DATA_WIDTH-1 downto 0)
  );

-- Declarations

end uart_rx ;

architecture rtl of uart_rx is
  signal data_r : std_logic_vector(DATA_WIDTH+1 downto 0); -- start, data, stop
  signal bit_count_r     : integer range 0 to DATA_WIDTH+1;
  signal sample_count_r  : integer range 0 to BIT_SAMPLES-1;
  signal sample_bit : std_logic := '0';
  signal serial_s : std_logic := '0';
  signal serial_r : std_logic := '0';
  signal busy_r : std_logic := '0';
  signal ready_r  : std_logic := '0';
  signal error_r  : std_logic := '0';
begin

  -- sample input data with two levels of FF to avoid metaestability effects
  sync_in_proc : process(rst_ni, clk_i)
  begin
    if (rst_ni = '0') then
      serial_s <= '1';
      serial_r <= '1';
    elsif (rising_edge(clk_i)) then       
      serial_s <= serial_i;
      serial_r <= serial_s;
    end if;    
  end process;
  
  -- busy flag control
  busy_proc : process(rst_ni, clk_i)
  begin
    if (rst_ni = '0') then
      busy_r <= '0';
    elsif (rising_edge(clk_i)) then       
      if (enable_i = '1' and busy_r = '0' and serial_r = '0') then
        -- start counting samples when serial_r is '0' and not already counting
        busy_r <= '1';
      elsif (ready_r = '1' or error_r = '1') then
        -- stop when a byte is finished (ok or in error)
        busy_r <= '0';
      end if;
    end if;    
  end process;
  
  -- sample counter at fast baudrate, sync in the middle
  sample_count_proc : process(rst_ni, clk_i)
  begin
    if (rst_ni = '0') then
      sample_count_r  <= 0;
    elsif (rising_edge(clk_i)) then       
      if (busy_r = '0' and serial_r = '1') then
        sample_count_r  <= 0;
      elsif (fast_baud_i = '1') then
        if (sample_count_r < BIT_SAMPLES-1) then
          sample_count_r <= sample_count_r + 1;
        else
          sample_count_r <= 0;
        end if;
      end if;
    end if;    
  end process;

  -- sample pulse for each bit synchronized in the middle of each bit
  sample_bit_proc : process(fast_baud_i, sample_count_r)
  begin
    sample_bit <= '0';
    if (fast_baud_i = '1' and sample_count_r = (BIT_SAMPLES+1)/2-1) then
      sample_bit <= '1';
    end if;    
  end process;

  -- count bits on each bit sample pulse
  bit_count_proc : process(rst_ni, clk_i)
  begin
    if (rst_ni = '0') then
      bit_count_r <= 0;
    elsif (rising_edge(clk_i)) then       
      if (busy_r = '0') then
        bit_count_r <= 0;
      elsif (sample_bit = '1') then
        if (bit_count_r = DATA_WIDTH+1) then
          bit_count_r <= 0;
        else
          bit_count_r <= bit_count_r + 1;
        end if;
      end if;
    end if;    
  end process;

  -- sample data and generate data valid pulse
  data_o <= data_r(DATA_WIDTH downto 1);
  ready_o <= ready_r;
  error_o <= error_r;
  
  data_proc : process(rst_ni, clk_i)
  begin
    if (rst_ni = '0') then
      data_r  <= (others => '0');
      ready_r <= '0';
      error_r <= '0';
    elsif (rising_edge(clk_i)) then       
      ready_r <= '0';
      error_r <= '0';
      if (sample_bit = '1') then
        -- saves the serial data
        data_r(bit_count_r) <= serial_r;
        -- when the last bit comes generate error or valid pulse and latch data
        if (bit_count_r = DATA_WIDTH+1) then
          -- check start and stop bits
          if (data_r(0) = '0' and serial_r = '1') then 
            ready_r <= '1';
          else
            error_r <= '1';
          end if;
        end if;
      end if;
    end if;    
  end process;
                        
end architecture rtl;
