----------------------------------------------------------------------------------
-- Company:  Instituto Balseiro
-- Engineer: Guillermo Guichal
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Ejemplos b�sicos de dise�o VHDL
-- 
-- Dependencies: None.
-- 
-- Revision: 2016-02-18.01
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity tb_uart_gen is
    port ( uart_o  : out std_logic);
end tb_uart_gen;

architecture sim of tb_uart_gen is

--    constant TSYMBOL : time := 104.167 us; --9600
    constant TSYMBOL : time := 8.681 us; --115200
    --constant RX_BYTE : std_logic_vector(7 downto 0) := x"A5";
    subtype td_rx_byte is std_logic_vector(7 downto 0); 

    type td_rx_byte_list is array (natural range <>) of td_rx_byte;
    
    constant rx_bytes : td_rx_byte_list := (
       x"01", -- cmd ctrl
       x"01",   -- rst
       x"03", -- cmd led
       x"01",
       x"02", -- cmd color R
       x"ff",
       x"00",
       x"00",
       x"01",   -- time 
       x"02", -- cmd color G
       x"00",
       x"ff",
       x"00",
       x"01",   -- time 
       x"02", -- cmd color B
       x"00",
       x"00",
       x"ff",
       x"01",   -- time 
       x"02", -- cmd color 0f0f0f
       x"0f",
       x"0f",
       x"0f",
       x"01",   -- time        x"01", -- cmd ctrl
       x"01", -- cmd ctrl       
       x"02",   -- enable
       x"03", -- cmd led
       x"0A"
       );   
    
begin

    process
      variable v_rx_byte : std_logic_vector(7 downto 0);
    begin
      uart_o <= '1';   
      wait for 100 * TSYMBOL;
      
      for i in 0 to rx_bytes'length-1 loop
        v_rx_byte := rx_bytes(i); 
        -- Start bit
        uart_o <= '0';        
        wait for TSYMBOL;
        -- Data bits
        for j in 0 to 7 loop
          uart_o <= v_rx_byte(j);
          wait for TSYMBOL;
        end loop;
        -- Stop bit
        uart_o <= '1'; 
        wait for TSYMBOL; -- Stop bit
        --wait for 0.5 * TSYMBOL;
        -- Wait time between bytes 
        --wait for 5*TSYMBOL; -- Allows easier debug in sims
        
      end loop;

      wait;      
    end process;    
      
end sim;
