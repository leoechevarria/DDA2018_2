# case1

                case lsel_r is
                    when "00"   =>  led_o <= "000" & toggle_r;
                    when "01"   =>  led_o <= "00" & toggle_r & "0";
                    when "10"   =>  led_o <= "0" & toggle_r & "00";
                    when others =>  led_o <= toggle_r & "000";
                end case;
![1536153590979](png\1536153590979.png)

![1536153740411](png\1536153740411.png)



![1536161472307](png\1536161472307.png)

## PAR Results

![1536173910054](png\1536173910054.png)

![1536173947999](png\1536173947999.png)



# for_1proc

```
            for i in 0 to 3 loop
                if unsigned(lsel_r) = i then
                    led_o(i) <= toggle_r;
                else    
                    led_o(i) <= '0';
                end if;    
            end loop;
```

![1536154905724](D:/Proyectos/Balseiro/DDA2018_2_interno/tp2_demux_pruebas_sintesis/docs/png/1536154905724.png)

![1536154990822](D:/Proyectos/Balseiro/DDA2018_2_interno/tp2_demux_pruebas_sintesis/docs/png/1536154990822.png)

![1536162175025](D:/Proyectos/Balseiro/DDA2018_2_interno/tp2_demux_pruebas_sintesis/docs/png/1536162175025.png)

## PAR Results

![1536174821550](png\1536174821550.png)

![1536174858281](png\1536174858281.png)



# for_2proc

```
process (lsel_i, toggle_i)
begin    
    for i in 0 to 3 loop
        if unsigned(lsel_r) = i then
            led_comb(i) <= toggle_r;
        else    
            led_comb(i) <= '0';
        end if;    
    end loop;
end process;

process (clk_i)
begin    
    if rising_edge(clk_i) then
        if rst_i = '1' then
            led_o <= (others => '0');
        else
            led_o <= led_comb;  
        end if;    
    end if;    
end process;
```

![1536162319691](D:/Proyectos/Balseiro/DDA2018_2_interno/tp2_demux_pruebas_sintesis/docs/png/1536162319691.png)

![1536155259151](D:/Proyectos/Balseiro/DDA2018_2_interno/tp2_demux_pruebas_sintesis/docs/png/1536155259151.png)



![536160858826](D:/Proyectos/Balseiro/DDA2018_2_interno/tp2_demux_pruebas_sintesis/docs/png/1536160858826.png)



# case_const1

                led_o <= (others => '0');
                if toggle_r = '1' then
                    case lsel_r is
                        when "00"   =>  led_o <= "0001";
                        when "01"   =>  led_o <= "0010";
                        when "10"   =>  led_o <= "0100";
                        when others =>  led_o <= "1000";
                    end case;
                end if;
![1536154505435](png\1536154505435.png)





![1536154615351](png\1536154615351.png)



![1536161721949](png\1536161721949.png)









# case_const2

                if toggle_r = '1' then
                    case lsel_r is
                        when "00"   =>  led_o <= "0001";
                        when "01"   =>  led_o <= "0010";
                        when "10"   =>  led_o <= "0100";
                        when others =>  led_o <= "1000";
                    end case;
                else
                    led_o <= (others => '0');
                end if;
![1536154709080](png\1536154709080.png)



![1536154796186](png\1536154796186.png)



![1536161851832](png\1536161851832.png)

