# Repositorio para la materia Diseño Digital Avanzado

## Instituto Balseiro / UNCuyo / CNEA - Segundo Semestre 2018

### Estructura de directorios

* docs

  * clases: diapositivas de las clases teóricas
  * misc: todo tipo de documentos de terceros (notas de aplicación, datasheets, artículos, etc.)
  * notas: ayudas extras a temáticas no abordadas directamente en las clases teóricas, pero que pueden resultar de utilidad para entender conceptos o resolver ciertos ejercicios 
  * tp: enunciados de los trabajos prácticos

* misc: archivos sin una categoría clara (por ejemplo, board files para placas de desarrollo)

* src

  * constraints: archivos de asignación de pines y timing
  * examples: ejemplos de código de todo tipo
  * lib: bloques de código que serán reutilizados en más de un trabajo práctico
  * sim: código a utilizar a la hora de verificar (generadores de reloj, ejemplos de testbench, etc.)
  * tp: código necesario para completar cada trabajo práctico

  